package py.com.ande.servernis.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pbbattilana
 */
public class NIS {
    /**
     * Map: Integer key(idNis) | List<Integer> consumo | estado
     * List: Integer:consumo | Integer: 0->Inactivo, 1->Acitvo
     * 123 : 500 - 1
     * 234 : 625 - 1
     * 345 : 0 - 0
     */
    private Map<Integer,List<Integer>> listNIS;

    public NIS() {
        Map<Integer,List<Integer>> listNIS = new HashMap<>();
        List<Integer> detalle = new ArrayList<>();
        detalle.add(500);
        detalle.add(1);
        listNIS.put(123, detalle);
        detalle.clear();
        detalle.add(625);
        detalle.add(1);
        listNIS.put(234, detalle);
        detalle.clear();
        detalle.add(0);
        detalle.add(0);
        listNIS.put(345, detalle);
        detalle.clear();
        detalle.add(125);
        detalle.add(1);
        listNIS.put(456, detalle);
        detalle.clear();
        detalle.add(0);
        detalle.add(0);
        listNIS.put(567, detalle);
        detalle.clear();
        detalle.add(527);
        detalle.add(1);
        listNIS.put(678, detalle);
        detalle.clear();
        detalle.add(0);
        detalle.add(0);
        listNIS.put(789, detalle);
        detalle.clear();
        detalle.add(845);
        detalle.add(1);
        listNIS.put(555, detalle);
        this.listNIS = listNIS;
    }

    public NIS(Map<Integer, List<Integer>> listNIS) {
        this.listNIS = listNIS;
    }
    
    public Map<Integer, List<Integer>> getListNIS() {
        return listNIS;
    }

    public void setListNIS(Map<Integer, List<Integer>> listNIS) {
        this.listNIS = listNIS;
    }
    
    public void registrarConsumo(Integer key, Integer consumo){
        List<Integer> detalle = new ArrayList<>();
        detalle.add(consumo); detalle.add(1);
        this.listNIS.put(key, detalle);
    }
    
    public void ordenConexion(Integer key){
        List<Integer> detalle = new ArrayList<>();
        detalle.add(0); detalle.add(1);
        this.listNIS.put(key, detalle);
    }
    
    public void ordenDesconexion(Integer key){
        List<Integer> detalle = new ArrayList<>();
        detalle.add(0); detalle.add(0);
        this.listNIS.put(key, detalle);
    }
    
    public List<Integer> listarNISActivos(){
        List<Integer> lista = new ArrayList<>();
        for (Iterator<Map.Entry<Integer, List<Integer>>> entries = this.listNIS.entrySet().iterator(); entries.hasNext();) {
            Map.Entry<Integer, List<Integer>> entry = entries.next();
            if(entry.getValue().get(1) == 1){
                lista.add(entry.getKey());
            }
        }
        return lista;
    }
    
    public List<Integer> listarNISInactivos(){
        List<Integer> lista = new ArrayList<>();
        for (Iterator<Map.Entry<Integer, List<Integer>>> entries = this.listNIS.entrySet().iterator(); entries.hasNext();) {
            Map.Entry<Integer, List<Integer>> entry = entries.next();
            if(entry.getValue().get(1) == 0){
                lista.add(entry.getKey());
            }
        }
        return lista;
    }
    
}
