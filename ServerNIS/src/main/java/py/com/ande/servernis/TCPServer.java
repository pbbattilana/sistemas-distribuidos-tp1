package py.com.ande.servernis;

import java.util.concurrent.TimeUnit;
import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.com.ande.servernis.dao.NIS;

public class TCPServer {

    private final static Logger LOG_MONITOREO = Logger.getLogger("Servidor TCP NIS");
    
    public static void main(String[] args) throws Exception {

        int puertoServidor = 4444;
        int tiempo_procesamiento_miliseg = 2000;

        try {
            tiempo_procesamiento_miliseg = Integer.parseInt(args[0]);
        } catch (Exception e1) {
            LOG_MONITOREO.log(Level.INFO, "Se omite el argumento, tiempo de procesamiento " + tiempo_procesamiento_miliseg + ". Ref: " + e1);
        }

        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(puertoServidor);
        } catch (IOException e) {
            LOG_MONITOREO.log(Level.WARNING, "No se puede abrir el puerto: " + puertoServidor + ".");
            System.exit(1);
        }
        LOG_MONITOREO.log(Level.INFO, "Puerto abierto: " + puertoServidor + ".");
        Socket clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            LOG_MONITOREO.log(Level.WARNING, "Fallo el accept().");
            System.exit(1);
        }

        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        clientSocket.getInputStream()));

        out.println("Bienvenido!\t"
                .concat("Mensaje descriptivo de opciones\t")
                .concat("Opcion1\t")
                .concat("Opcion2\t")
        );
        String inputLine = "-1", outputLine;
        NIS nis = new NIS();
        while (inputLine.equals("0")) {
            inputLine = in.readLine();
            LOG_MONITOREO.log(Level.INFO, "Mensaje recibido: " + inputLine);
            outputLine = "Respuesta igual al recibido: " + inputLine;
            //Aca el switch. ejemplo:
            //switch(inputLine){
            //case "1":
            //outputLine = nis.listarNISActivos();
            //}
            TimeUnit.MILLISECONDS.sleep(tiempo_procesamiento_miliseg);

            out.println(outputLine);
        }
        

        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }
}
